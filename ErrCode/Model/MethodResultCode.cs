﻿namespace ErrCode.Model
{
    /// <summary>
    /// "方法结果"代码
    /// author:xiaoy
    /// 2015-09-23 13:46:32 
    /// </summary>
    public class MethodResultCode
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public int MethodId { get; set; }
    }
}
