﻿using System.Collections.Generic;
using ErrCode.Dal;
using ErrCode.Model;

namespace ErrCode.Bll
{
    public class ProjectBll
    {
        readonly ProjectDal _proDal = new ProjectDal();
        public List<SystemId> GetAllProjects()
        {
            return _proDal.GetAllProjects();
        }

        public int InsertSystem(string description)
        {
            var systemId = _proDal.InsertSystem(description);
            return systemId > 0 ? systemId : 0;
        }

        public SystemId GetSystemIdByCode(char code)
        {
            SystemId systemId = _proDal.GetSystemIdByCode(code);
            return systemId;
        }

    }
}
