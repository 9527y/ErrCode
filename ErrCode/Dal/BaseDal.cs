﻿using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace ErrCode
{
    public class BaseDal
    {
        /// <summary>
        /// 账户系统数据库
        /// 2015年6月4日20:33:05
        /// 马谦
        /// </summary>
        public IDbConnection ErrCodeSysConnection
        {
            get
            {
                string strCon = ConfigurationManager.ConnectionStrings["as_db"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(strCon);
                return connection;
            }
        }

    }
}
