﻿using ErrCode.Frms;

namespace ErrCode
{
    public class CommonService
    {
        public readonly static char[] ProCode =
        {
            '1','2','3','4','5','6','7','8','9','0',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        };

        public static int CurrSystemId { set; get; }
        public static char CurrSystemCode { set; get; }
        public static SelectSystemFrm SelectSystemFrm { set; get; }
    }
}
