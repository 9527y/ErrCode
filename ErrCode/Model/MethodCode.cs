﻿namespace ErrCode.Model
{
    /// <summary>
    /// "方法"代码
    /// author:xiaoy
    /// 2015-09-23 13:46:32 
    /// </summary>
    public class MethodCode
    {
        public int Id { get; set; }
        public string MethodName { get; set; }
        public int Seed { get; set; }
        public int SystemId { set; get; }
        public int SystemMethodId { get; set; }
    }
}
