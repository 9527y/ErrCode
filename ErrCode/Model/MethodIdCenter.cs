﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrCode.Model
{
    public class MethodIdCenter
    {

        public int Id { set; get; }
        public int MethodSeed { set; get; }
        public int SystemId { set; get; }
    }
}
