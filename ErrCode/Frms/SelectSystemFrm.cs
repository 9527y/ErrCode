﻿using System;
using System.Linq;
using System.Windows.Forms;
using ErrCode.Bll;
using ErrCode.Model;

namespace ErrCode.Frms
{
    public partial class SelectSystemFrm : BaseFrm
    {
        ProjectBll projectBll = new ProjectBll();

        public SelectSystemFrm()
        {
            InitializeComponent();
            LoadProjects();
        }

        private void btnSelectPro_Click(object sender, EventArgs e)
        {
            if (cmbProList.SelectedIndex < 0)
            {
                return;
            }
            int currId;
            if (!int.TryParse(cmbProList.SelectedValue.ToString(), out currId))
            {
                return;
            }
            var selectedItem = cmbProList.SelectedItem as SystemId;
            if (selectedItem == null)
            {
                return;
            }

            CommonService.CurrSystemCode = selectedItem.SystemCode;
            CommonService.CurrSystemId = currId;
            MainFrm main = new MainFrm();
            CommonService.SelectSystemFrm = this;
            main.Show();
            Hide();
        }

        private void btnAddPro_Click(object sender, EventArgs e)
        {
            string projectName = txtAddPro.Text.Trim();
            CommonService.CurrSystemId = projectBll.InsertSystem(projectName);
            LoadProjects();
        }

        private void LoadProjects()
        {
            cmbProList.DataSource = projectBll.GetAllProjects();
            cmbProList.DisplayMember = "Description";
            cmbProList.ValueMember = "Id";
        }
    }
}
