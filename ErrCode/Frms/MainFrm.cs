﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ErrCode.Bll;

namespace ErrCode.Frms
{
    public partial class MainFrm : BaseFrm
    {
        MessageCodeBll bll = new MessageCodeBll();
        public string MethodName { get; set; }
        public string ResultMsg { get; set; }


        public MainFrm()
        {
            InitializeComponent();
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            GetMethodResultCode();
        }
        public void GetMethodResultCode()
        {
            string code = bll.GetMessageResultCode(MethodName, ResultMsg).ToString();
            txtCode.Text = code;
            Clipboard.SetDataObject(code);
            toolStrip.Text = String.Format("已复制-{0}", code);
        }

        private void txtMethod_TextChanged(object sender, EventArgs e)
        {
            var t = sender as TextBox;
            if (t != null)
            {
                MethodName = t.Text;
            }
        }


        private void txtMsg_TextChanged(object sender, EventArgs e)
        {
            var t = sender as TextBox;
            if (t != null)
            {
                ResultMsg = t.Text;
            }
        }

        private void txtMethod_Enter(object sender, EventArgs e)
        {
            toolStrip.Text = "";
        }

        private void queryBtn_Click(object sender, EventArgs e)
        {
            string code = queryCode.Text;
            string methodStr = bll.GetMethodNameByCode(code);
            queryMethodName.Text = methodStr;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            Application.Exit();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            CommonService.SelectSystemFrm.Show();
        }
    }
}
