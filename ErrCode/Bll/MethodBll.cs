﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrCode.Dal;

namespace ErrCode.Bll
{
    public class MethodBll
    {
        public static int NextId()
        {
            MethodIdCenterDal dal = MethodIdCenterDal.Instance;
            return dal.NextMethodId();
        }
    }
}
