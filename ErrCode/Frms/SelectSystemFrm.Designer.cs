﻿namespace ErrCode.Frms
{
    partial class SelectSystemFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbProList = new System.Windows.Forms.ComboBox();
            this.btnSelectPro = new System.Windows.Forms.Button();
            this.txtAddPro = new System.Windows.Forms.TextBox();
            this.btnAddPro = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择项目：";
            // 
            // cmbProList
            // 
            this.cmbProList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProList.FormattingEnabled = true;
            this.cmbProList.Location = new System.Drawing.Point(84, 10);
            this.cmbProList.Name = "cmbProList";
            this.cmbProList.Size = new System.Drawing.Size(282, 20);
            this.cmbProList.TabIndex = 1;
            // 
            // btnSelectPro
            // 
            this.btnSelectPro.Location = new System.Drawing.Point(382, 8);
            this.btnSelectPro.Name = "btnSelectPro";
            this.btnSelectPro.Size = new System.Drawing.Size(75, 23);
            this.btnSelectPro.TabIndex = 2;
            this.btnSelectPro.Text = "选择";
            this.btnSelectPro.UseVisualStyleBackColor = true;
            this.btnSelectPro.Click += new System.EventHandler(this.btnSelectPro_Click);
            // 
            // txtAddPro
            // 
            this.txtAddPro.Location = new System.Drawing.Point(84, 60);
            this.txtAddPro.Name = "txtAddPro";
            this.txtAddPro.Size = new System.Drawing.Size(282, 21);
            this.txtAddPro.TabIndex = 3;
            // 
            // btnAddPro
            // 
            this.btnAddPro.Location = new System.Drawing.Point(382, 58);
            this.btnAddPro.Name = "btnAddPro";
            this.btnAddPro.Size = new System.Drawing.Size(75, 23);
            this.btnAddPro.TabIndex = 2;
            this.btnAddPro.Text = "增加";
            this.btnAddPro.UseVisualStyleBackColor = true;
            this.btnAddPro.Click += new System.EventHandler(this.btnAddPro_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "添加项目：";
            // 
            // SelectSystemFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 97);
            this.Controls.Add(this.txtAddPro);
            this.Controls.Add(this.btnAddPro);
            this.Controls.Add(this.btnSelectPro);
            this.Controls.Add(this.cmbProList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SelectSystemFrm";
            this.Text = "选择项目";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbProList;
        private System.Windows.Forms.Button btnSelectPro;
        private System.Windows.Forms.TextBox txtAddPro;
        private System.Windows.Forms.Button btnAddPro;
        private System.Windows.Forms.Label label2;
    }
}