﻿using System;
using ErrCode.Dal;
using ErrCode.Model;

namespace ErrCode.Bll
{
    public class MessageCodeBll
    {

        public readonly char ProjectCode;
        public readonly int ProjectId;
        public MessageCodeBll()
        {
            ProjectCode = CommonService.CurrSystemCode;
            ProjectId = CommonService.CurrSystemId;
        }

        MessageCodeDal dal = new MessageCodeDal();
        public string GetMessageResultCode(string methodName, string resultMsg)
        {
            //查询方法代码：
            string tmp = methodName.ToLower().Trim();
            int methodSeed;
            int resultSeed;
            MethodCode methodCode = dal.GetMethodCodeByMethodName(tmp, ProjectId);
            if (methodCode == null)
            {
                //插入，获取返回
                methodCode = new MethodCode()
                {
                    MethodName = methodName.Trim(),
                    Seed = 1,
                    SystemId = ProjectId,
                    SystemMethodId = MethodBll.NextId()
                };
                methodCode.Id = dal.InsertMethodCode(methodCode);
            }
            methodSeed = methodCode.SystemMethodId;
            resultSeed = methodCode.Seed;
            methodCode.Seed += 1;
            //更新Method
            var updateResult = dal.UpdateMethodCodeSeed(methodCode);
            if (updateResult <= 0)
            {
                throw new Exception("更新种子出错");
            }
            //根据MethodCode插入数据
            MethodResultCode resultCode = new MethodResultCode
            {
                MethodId = methodCode.Id,
                Message = resultMsg,
                Code = resultSeed
            };
            resultCode.Id = dal.InsertMethodResultCode(resultCode);
            string methodCodeStr = methodSeed.ToString();
            var zSize = 3 - methodCodeStr.Length;
            string result = ProjectCode.ToString();
            for (int i = 0; i < zSize; i++)
            {
                result += "0";
            }
            result += methodCodeStr;
            zSize = 2 - resultCode.Code.ToString().Length;
            for (int i = 0; i < zSize; i++)
            {
                result += "0";
            }
            result += resultCode.Code;
            if (result.Length > 6)
            {
                throw new Exception("代码位数超限");
            }
            return result;
        }

        /// <summary>
        /// 根据code获取方法
        /// author:xiaoy
        /// 2015-10-14 12:44:55 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetMethodNameByCode(string code)
        {
            if (string.IsNullOrEmpty(code) || code.Length != 6)
            {
                return "Error";
            }
            char systemCode = code[0];
            //获取系统Id
            ProjectBll projectBll = new ProjectBll();
            SystemId systemId = projectBll.GetSystemIdByCode(systemCode);
            //获取方法名：
            string methodStr = code.Substring(code.Length - 5, 3);
            int systemMethodId = Convert.ToInt32(methodStr);
            MethodCode mc = dal.GetMethodCodeByMethodId(systemMethodId, systemId.Id);
            if (mc != null)
            {
                return mc.MethodName;
            }
            return "Error";
        }
    }
}
