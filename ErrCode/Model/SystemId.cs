﻿namespace ErrCode.Model
{
    public class SystemId
    {
        public int Id { set; get; }
        public char SystemCode { set; get; }
        public string Description { set; get; }
    }
}
